package ernestoalarcon.com.mytouristguide;


import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private String[] optionsMenu;
    private ActionBarDrawerToggle drawerToggle;

    private CharSequence sectionTitle;
    private CharSequence appTitle;

    private FragmentListView fListView;
    private FragmentDescription fDescription;
    private FragmentMap fMap;

    private boolean listview;
    private boolean description;
    private boolean map;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeFragments();

        checkSavedInstaceState(savedInstanceState);

        optionsMenu = new String[] {"Touristic Points", "Description", "Map"};
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.left_drawer);

        drawerList.setAdapter(new ArrayAdapter<>(getSupportActionBar().getThemedContext(), android.R.layout.simple_list_item_1, optionsMenu));
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fListView, "listView").commit();
                        break;
                    case 1:
                        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fDescription, "description").commit();
                        break;
                    case 2:
                        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fMap, "map").commit();
                        break;
                }

                drawerList.setItemChecked(position, true);

                sectionTitle = optionsMenu[position];
                getSupportActionBar().setTitle(sectionTitle);
                drawerLayout.closeDrawer(drawerList);
            }
        });

        sectionTitle = getTitle();
        appTitle = getTitle();

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                super.onDrawerClosed(drawerView);
                getSupportActionBar().setTitle(sectionTitle);
                ActivityCompat.invalidateOptionsMenu(MainActivity.this);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(appTitle);
                ActivityCompat.invalidateOptionsMenu(MainActivity.this);
            }
        };

        drawerLayout.setDrawerListener(drawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_settings:
                Toast.makeText(this, "TODO", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_search:
                Toast.makeText(this, "TODO", Toast.LENGTH_SHORT).show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        boolean openMenu = drawerLayout.isDrawerOpen(drawerList);

        if (openMenu)
            menu.findItem(R.id.action_search).setVisible(false);
        else
            menu.findItem(R.id.action_search).setVisible(true);

        return super.onPrepareOptionsMenu(menu);
    }

    public void initializeFragments(){

        if (fListView == null) {
            fListView = new FragmentListView();
        }

        if (fDescription == null) {
            fDescription = new FragmentDescription();
        }

        if (fMap == null){
            fMap = new FragmentMap();
        }
    }

    public void checkSavedInstaceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fListView, "listView").commit();
            swichFragment("listview");

        } else if (savedInstanceState != null) {
            if (savedInstanceState.getString("current_fragment").equals("description")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fDescription, "description").commit();
                swichFragment("description");

            } else if (savedInstanceState.getString("current_fragment").equals("map")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fMap, "map").commit();
                swichFragment("map");
            }
        }
    }

    public void swichFragment(String fragmentName){
        if (fragmentName == "listview"){
            listview = true;
            description = false;
            map = false;
        } else if (fragmentName == "description") {
            listview = false;
            description = true;
            map = false;
        } else if (fragmentName == "map"){
            listview = false;
            description = false;
            map = true;
        }
    }

}
